package main

import (
	"fmt"
	"github.com/heidricha/stringutil"
)

func main() {
	fmt.Println(stringutil.Reverse("!gnaLoG ,olleH"))
}
