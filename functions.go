// _Functions_ are central in Go. We'll learn about
// functions with a few different examples.

package main

import "fmt"

// Here's a function that takes two `int`s and returns
// their sum as an `int`.
func plus(a int, b int) int {

	// Go requires explicit returns, i.e. it won't
	// automatically return the value of the last
	// expression.
	return a + b
}

// When you have multiple consecutive parameters of
// the same type, you may omit the type name for the
// like-typed parameters up to the final parameter that
// declares the type.
func plusPlus(a, b, c int) int {
	return a + b + c
}

func revStr3(s string) string {
	r := []byte(s)
	for i := range r[:len(s)/2] {
		r[i], r[len(s)-i-1] = r[len(s)-i-1], r[i]
	}
	return string(r)
}

func revStr2(s string) string {
	r := []byte(s)
	for first, last := 0, len(s)-1; first < last; first, last = first+1, last-1 {
		r[first], r[last] = r[last], r[first]
	}
	return string(r)
}

func revStr(s string) string {
	switch length := len(s); length {
	case 0, 1:
		{
			return s
		}
	default:
		{
			return s[length-1:] + revStr(s[:length-1])
		}
	}
}

func main() {

	// Call a function just as you'd expect, with
	// `name(args)`.
	res := plus(1, 2)
	fmt.Println("1+2 =", res)

	res = plusPlus(1, 2, 3)
	fmt.Println("1+2+3 =", res)

	s := string("Arám ász kovács a jeled a Tallium ah s e mellek kellemes hamu illata delej a csávók számára")
	//s := string(make([]byte, 10000))
	fmt.Println(revStr(s))
	fmt.Println(revStr2(s))
	fmt.Println(revStr3(s))
}
