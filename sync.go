// We can use channels to synchronize execution
// across goroutines. Here's an example of using a
// blocking receive to wait for a goroutine to finish.

package main

import "fmt"
import "time"

// This is the function we'll run in a goroutine. The
// `done` channel will be used to notify another
// goroutine that this function's work is done.
func worker(done chan bool) {
	s := time.Second * 3
	fmt.Printf("wait %s...", time.Duration(s))
	time.Sleep(s)
	fmt.Println("done")

	// Send a value to notify that we're done.
	done <- true
}

func main() {

	// Start a worker goroutine, giving it the channel to
	// notify on.
	done := make(chan bool, 3)
	signal := make(chan bool)

	go worker(done)

	go func(s chan bool) {
		i := 0
		for stop := false; !stop; i++ {
			select {
			case stop = <-s:

			default:
				fmt.Printf("still waiting (%d)\n", i)
				time.Sleep(time.Millisecond * 500)
			}
		}
		fmt.Println("stop received")
	}(signal)
	// Block until we receive a notification from the
	// worker on the channel.
	<-done
	signal <- true
}
